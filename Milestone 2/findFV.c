#include<stdio.h>
#include<math.h>
double fv(double rate, unsigned int nperiods, double pv);
int main()
{
  double rate,pv;
  unsigned int nperiods;
  printf("Enter a Rate:");
  scanf("%lf",&rate);
  printf("\nEnter nPeriods :");
  scanf("%u",&nperiods);
  printf("\nEnter pv:");
  scanf("%lf",&pv);
  printf("Future value of an invenstment = %lf ", fv(rate,nperiods,pv));
  return 0;
}
double fv(double rate, unsigned int nperiods, double pv)
{
   double fv;
   fv=pv*(pow((1+rate),nperiods));
   return fv;
}