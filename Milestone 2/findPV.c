#include<stdio.h>
#include<math.h>
double pv(double rate, unsigned int nperiods, double fv);
int main()
{
  double rate,fv;
  unsigned int nperiods;
  printf("Enter a Rate:");
  scanf("%lf",&rate);
  printf("\nEnter nPeriods :");
  scanf("%u",&nperiods);
  printf("\nEnter pv:");
  scanf("%lf",&fv);
  printf("Future value of an invenstment = %lf ", pv(rate,nperiods,fv));
  return 0;
}
double pv(double rate, unsigned int nperiods, double fv)
{
   double pv;
   pv=fv*(pow((1+rate),nperiods));
   return pv;
}