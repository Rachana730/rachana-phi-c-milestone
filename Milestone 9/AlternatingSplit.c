#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void MoveNode(struct List** dest_ref, struct List** source_ref){
    struct List* newNode = *source_ref;
    assert(newNode != NULL);
    *source_ref = newNode->next; 
    newNode->next = *dest_ref; 
    *dest_ref = newNode;
    
}

void AlternatingSplit(struct List* source,struct List** a_ref, struct List** b_ref) {

    struct List* a = NULL; 
    struct List * b = NULL;
    struct List * current = source;
    while (current != NULL) {
        MoveNode(&a, &current); 
        if (current != NULL) {
        MoveNode(&b, &current); 
        }
    }
    *a_ref = a;
    *b_ref = b;
}

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* source = NULL;
    struct List* a = NULL;
    struct List* b = NULL;
    
    push(&source, 5); 
    push(&source, 1); 
    push(&source, 1);  
    push(&source, 1); 
    push(&source, 5);
    push(&source, 5);
    
    AlternatingSplit(source, &a, &b);
     
    printf("Two nodes created successfully" ); 
} 