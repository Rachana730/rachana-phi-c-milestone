#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void MoveNode(struct List** dest_ref, struct List** source_ref){
    struct List* newNode = *source_ref;
    assert(newNode != NULL);
    *source_ref = newNode->next; 
    newNode->next = *dest_ref; 
    *dest_ref = newNode;
    
}

struct List* ShuffleMerge(struct List* a, struct List* b){
    struct List dummy;
    struct List* tail = &dummy;
    dummy.next = NULL;
    while (1) {
        if (a==NULL) {
            tail->next = b;
            break;
        }
        else if (b==NULL) {
            tail->next = a;
            break;
        }
        else { 
            MoveNode(&(tail->next), &a);
            tail = tail->next;
            MoveNode(&(tail->next), &b);
            tail = tail->next;
        }
    }
    return(dummy.next);
}

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* a = NULL;
    struct List* b = NULL;
    
    push(&a, 5); 
    push(&a, 1); 
    push(&a, 1);  
    push(&b, 1); 
    push(&b, 5);
    push(&b, 5);
    
    ShuffleMerge(a,b);
     
    printf("Two nodes merged successfully" ); 
} 